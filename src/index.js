const { app, BrowserWindow, ipcMain } = require('electron');
const { MongoClient, MaxKey } = require('mongodb');
const path = require('path');


// Handle creating/removing shortcuts on Windows when installing/uninstalling.
if (require('electron-squirrel-startup')) { // eslint-disable-line global-require
  app.quit();
}

app.setLoginItemSettings({
  openAtLogin: true,
  path: app.getPath("exe"),
});

const categoryList = [
  "the_gioi",
  "thoi_su",
  "kinh_doanh",
  "giai_tri",
  "the_thao",
  "phap_luat",
  "giao_duc",
  "tin_moi_nhat",
  "suc_khoe",
  "doi_song",
  "khoa_hoc",
  "cong_nghe",
  "so_hoa"
]
let cols;
let records = {
  "the_gioi": undefined,
  "thoi_su": undefined,
  "kinh_doanh": undefined,
  "giai_tri": undefined,
  "the_thao": undefined,
  "phap_luat": undefined,
  "giao_duc": undefined,
  "tin_moi_nhat": undefined,
  "suc_khoe": undefined,
  "doi_song": undefined,
  "khoa_hoc": undefined,
  "cong_nghe": undefined,
  "so_hoa": undefined,
};
const currentValue = {
  "the_gioi": MaxKey(),
  "thoi_su": MaxKey(),
  "kinh_doanh": MaxKey(),
  "giai_tri": MaxKey(),
  "the_thao": MaxKey(),
  "phap_luat": MaxKey(),
  "giao_duc": MaxKey(),
  "tin_moi_nhat": MaxKey(),
  "suc_khoe": MaxKey(),
  "doi_song": MaxKey(),
  "khoa_hoc": MaxKey(),
  "cong_nghe": MaxKey(),
  "so_hoa": MaxKey(),
}
let countPrev = 0, countNext = 0, countClose = 0;
const THRESHOLD = 8;
const INTERVAL = 1000;

function clear() {
  countPrev = 0;
  countNext = 0;
  countClose = 0;
}

setInterval(() => {
  clear();
}, INTERVAL);

ipcMain.on("getNext", async (event, args) => {
  ++countNext;
  switch (args[0]) {
    case "selectCategory": {
      if (countNext === THRESHOLD) {
        clear();
        event.reply("switchCategory", true);
      }
      break;
    }
    case "readPaper": {
      if (countNext === THRESHOLD) {
        clear();
        await queryPageNext(currentValue[args[1]], args[1]);
        if (records[args[1]].length > 0) {
          event.reply("updateNews",
            [
              records[args[1]][0].tieu_de,
              records[args[1]][0].tom_tat,
              records[args[1]][0].img_url,
            ]
          );
        }
      }
      break;
    }
    case "modalSelect": {
      if (countNext === THRESHOLD) {
        clear();
        if (records[args[1]].length > 0) {
          event.reply("changeScreenType", [args[0], "modalPaper", records[args[1]][0].noi_dung_chi_tiet]);
        } else {
          event.reply("changeScreenType", [args[0], "modalPaper", "Không có bài viết."]);
        }
      }
      break;
    }
    case "modalPaper": {
      if (countNext === THRESHOLD) {
        clear();
        event.reply("scroll", true);
      }
      break;
    }
  }
});

ipcMain.on("getPrev", async (event, args) => {
  ++countPrev;
  switch (args[0]) {
    case "selectCategory": {
      if (countPrev === THRESHOLD) {
        clear();
        event.reply("switchCategory", false);
      }
      break;
    }
    case "readPaper": {
      if (countPrev === THRESHOLD) {
        clear();
        await queryPagePrev(currentValue[args[1]], args[1]);
        if (records[args[1]].length > 0) {
          event.reply("updateNews",
            [
              records[args[1]][0].tieu_de,
              records[args[1]][0].tom_tat,
              records[args[1]][0].img_url,
            ]
          );
        }
      }
      break;
    }
    case "modalSelect": {
      if (countPrev === THRESHOLD) {
        clear();
        event.reply("changeScreenType", [args[0], "selectCategory"]);
      }
      break;
    }
    case "modalPaper": {
      if (countPrev === THRESHOLD) {
        clear();
        event.reply("scroll", false);
      }
      break;
    }
  }
});

ipcMain.on("closedSignal", async (event, args) => {
  ++countClose;
  switch (args[0]) {
    case "selectCategory": {
      if (countClose === THRESHOLD) {
        clear();
        event.reply("changeScreenType", [args[0], "readPaper"]);
        if (records[args[1]].length > 0) {
          event.reply("updateNews",
            [
              records[args[1]][0].tieu_de,
              records[args[1]][0].tom_tat,
              records[args[1]][0].img_url,
            ]
          );
        } else {
          event.reply("updateNews",
            [
              'Không có bài viết',
              'Không có bài viết nào trong chủ đề này',
              '',
            ]
          );
        }
      }
      break;
    }
    case "readPaper": {
      if (countClose === THRESHOLD) {
        clear();
        event.reply("changeScreenType", [args[0], "modalSelect"]);
      }
      break;
    }
    case "modalPaper": {
      if (countClose === THRESHOLD) {
        clear();
        event.reply("changeScreenType", [args[0], "readPaper"]);
      }
      break;
    }
    case "modalSelect": {
      if (countClose === THRESHOLD) {
        clear();
        event.reply("changeScreenType", [args[0], "readPaper"]);
      }
      break;
    }
    case "tutorial": {
      if (countClose === THRESHOLD) {
        clear();
        event.reply("changeScreenType", [args[0], "selectCategory"]);
      }
      break;
    }
  }
});

async function queryPagePrev(startValue, category) {
  records[category] = await cols.find({ chu_de: category, _id: { $gt: startValue }, noi_dung_chi_tiet: { $ne: '' } })
    .sort({ _id: 1 })
    .limit(1).toArray();
  if (records[category].length > 0) {
    currentValue[category] = records[category][0]._id;
  }
}
async function queryPageNext(startValue, category) {
  records[category] = await cols.find({ chu_de: category, _id: { $lt: startValue }, noi_dung_chi_tiet: { $ne: '' } })
    .sort({ _id: -1 })
    .limit(1).toArray();
  if (records[category].length > 0) {
    currentValue[category] = records[category][0]._id;
  }
}

const createWindow = () => {
  // Create the browser window.
  const mainWindow = new BrowserWindow({
    webPreferences: {
      preload: path.join(__dirname, 'preload.js'),
      // devTools:false
    }
  });
  // and load the index.html of the app.
  mainWindow.loadFile(path.join(__dirname, 'index.html'));
  mainWindow.maximize();
  // Open the DevTools.
  // mainWindow.webContents.openDevTools();
};

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on('ready', async () => {
  const uri = "mongodb+srv://extreme45nm:congminh123@chatbot-test-0.p5qkv.mongodb.net/myFirstDatabase?retryWrites=true&w=majority";
  const dbName = "nkt-2021-tin-tuc";
  const colName = "tin-tuc";
  const client = new MongoClient(uri);
  await client.connect();
  cols = client.db(dbName).collection(colName);
  for (const category of categoryList) {
    await queryPageNext(currentValue[category], category);
  }
  createWindow();
});

// Quit when all windows are closed, except on macOS. There, it's common
// for applications and their menu bar to stay active until the user quits
// explicitly with Cmd + Q.
app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') {
    app.quit();
  }
});

app.on('activate', () => {
  // On OS X it's common to re-create a window in the app when the
  // dock icon is clicked and there are no other windows open.
  if (BrowserWindow.getAllWindows().length === 0) {
    createWindow();
  }
});
